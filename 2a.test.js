'use strct'

const solution = require('./2a.js')

const TESTDATA = [
    'abcdef',
    'bababc',
    'abbcde',
    'abcccd',
    'aabcdd',
    'abcdee',
    'ababab'
]

const EXPECTED = 12

it('should return expected ', () => {
    expect(solution(TESTDATA)).toEqual(EXPECTED)
})