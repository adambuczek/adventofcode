'use strct'

const solve = require('./1b.js')

const TESTDATA = [+3, +3, +4, -2, -4]
const EXPECTED = 10

it('should return expected ', () => expect(solve(TESTDATA)).toEqual(EXPECTED))
