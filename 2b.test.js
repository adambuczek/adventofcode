'use strct'

const _2b = require('./2b.js')
const solve = _2b.solve
const indexedStringDifference = _2b.indexedStringDifference
const removeElementByIndex = _2b.removeElementByIndex

const TESTDATA = [
    'abcde',
    'fghij',
    'klmno',
    'pqrst',
    'fguij',
    'axcye',
    'wvxyz',
]

const EXPECTED = 'fgij'

it('should return expected ', () => {
    expect(solve(TESTDATA)).toEqual(EXPECTED)
})

it('should return array difference considering positions', () => {
    expect(indexedStringDifference('fghij', 'fguij')).toEqual([{
        [1]: 'h',
        [2]: 'u',
        i: 2,
    }])
})

it('should slice away element by given index', () => {
    expect(removeElementByIndex('string', 3)).toEqual('strng')
    expect(removeElementByIndex('fghij', 2)).toEqual('fgij')
})
