'use strict'

const solve = array => array.reduce((acc, cur) => acc + Number(cur))

// console.info(solve(require('./1.json')))

module.exports = solve