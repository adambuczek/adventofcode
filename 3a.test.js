'use strct'

const {
    solve,
    parse,
    Rectangle,
    Point
} = require('./3a.js')

const TESTDATA = [
    '#1 @ 1,3: 4x4',
    '#2 @ 3,1: 4x4',
    '#3 @ 5,5: 2x2',
]

const EXPECTED = 4

describe('solve', () => {

    
    it('should return expected ', () => {
        expect(solve(TESTDATA)).toEqual(EXPECTED)
    })
    
})

describe('parse', () => {

    
    it('should parse input string ', () => {
        
        const T = TESTDATA.slice()
        
        expect(parse(T.shift())).toEqual({
            id: 1,
            left: 1, top: 3,
            width: 4, height: 4
        })
        
        expect(parse(T.shift())).toEqual({
            id: 2,
            left: 3, top: 1,
            width: 4, height: 4
        })
        
        expect(parse(T.shift())).toEqual({
            id: 3,
            left: 5, top: 5,
            width: 2, height: 2
        })
        
    })
    
})

describe('Rectangle', () => {
        
    const R1 = new Rectangle(3,1,4,4)
    const R2 = new Rectangle(1,3,4,4)
    const R3 = new Rectangle(5,5,2,2)
    const R4 = new Rectangle(4,4,2,2)
    const R5 = new Rectangle(1,1,7,7)
    const R6 = new Rectangle(1,1,2,4)

    const R1corners = Rectangle.fromCorners(new Point(3,1), new Point(7,5))

    test('rectangle should have expected props', () => 
        expect({
            lt: R1.leftTop,
            rb: R1.rightBottom
        })
            .toEqual({
                lt: new Point(3, 1),
                rb: new Point(7, 5)
            }))

    test('same rectangles should have the same props no matter how they were constructed', () => expect(R1).toEqual(R1corners))

    test('rectangles should overlap', () => expect(R1.overlaps(R2)).toEqual(true))
    test('rectangles should not overlap', () => expect(R1.overlaps(R3)).toEqual(false))
    test('rectangles should not overlap', () => expect(R2.overlaps(R3)).toEqual(false))

    test('intersection of two overlapping rectangles should be an expected rectangle', () => {
        expect(R4.intersection(R3)).toEqual( new Rectangle(5,5,1,1) )
        expect(R3.intersection(R4)).toEqual( new Rectangle(5,5,1,1) )
        expect(R2.intersection(R1)).toEqual( new Rectangle(3,3,2,2) )
        expect(R1.intersection(R2)).toEqual( new Rectangle(3,3,2,2) )
        expect(R1.intersection(R1)).toEqual(R1)
        expect(R5.intersection(R2)).toEqual(R2)
    })
    test('intersection of two non overlapping rectangles should be false', () => {
        expect(R1.intersection(R3)).toEqual(false)
        expect(R1.intersection(R6)).toEqual(false)
        expect(R6.intersection(R4)).toEqual(false)
    })

    const P1 = new Point(4, 4)
    const P2 = new Point(2, 2)
    test('point in rect', () => expect(R2.pointIn(P1)).toEqual(true))
    test('point not in rect', () => expect(R2.pointIn(P2)).toEqual(false))

})
