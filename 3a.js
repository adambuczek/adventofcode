'use strct'

class InputError extends Error {}

class Point {
    constructor(x, y) {
        this.x = x
        this.y = y
    }
}

class Rectangle {
    constructor (l, t, w, h) {
        if (w <= 0 || h <= 0) throw new InputError('Rectangle dimension must be greater than 0')
        this.leftTop = new Point(l, t)
        this.rightBottom = new Point(l + w, t + h)
    }

    overlaps (rect) {
        if (this.leftTop.x >= rect.rightBottom.x ||
            this.rightBottom.x <= rect.leftTop.x) return false
        if (this.leftTop.y >= rect.rightBottom.y ||
            this.rightBottom.y <= rect.leftTop.y) return false
        return true
    }

    intersection (rect) {
        if (this.overlaps(rect)) {
            
            const leftTopIn = this.pointIn(rect.leftTop)
            const rightBottomIn = this.pointIn(rect.rightBottom)
            const rightTopIn = this.pointIn(
                new Point(rect.rightBottom.x, rect.leftTop.y)
            )
            const leftBottomIn = this.pointIn(
                new Point(rect.leftTop.x, rect.rightBottom.y)
            )
            
            if (leftTopIn && rightBottomIn) return rect // rect is inside


            if (leftTopIn) return Rectangle.fromCorners(rect.leftTop, this.rightBottom)
            if (rightBottomIn) return Rectangle.fromCorners(this.leftTop, rect.rightBottom)
            
            if (rightTopIn)return Rectangle.fromCorners(
                new Point(this.leftTop.x, rect.leftTop.y),
                new Point(rect.rightBottom.x, this.rightBottom.y)
            )
            if (leftBottomIn) return Rectangle.fromCorners(
                new Point(rect.leftTop.x, this.leftTop.y),
                new Point(this.rightBottom.x, rect.rightBottom.y)
            )
            
            throw new InputError('Unexpected point outside the rect')            

        }
        return false
    }

    pointIn (point) {
        return (
            (this.leftTop.x <= point.x && point.x <= this.rightBottom.x) &&
            (this.leftTop.y <= point.y && point.y <= this.rightBottom.y)
        )
    }

    static fromCorners (leftTop, rightBottom) {
        if(
            !(leftTop instanceof Point) ||
            !(rightBottom instanceof Point)
        ) throw new InputError('Corners must be instances of Point')

        return new Rectangle(leftTop.x, leftTop.y, rightBottom.x - leftTop.x, rightBottom.y - leftTop.y)
    }
}

const parse = str => {
    const [id, left, top, width, height] = /^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$/
        .exec(str).splice(1, 5)
        .map(match => parseInt(match) )
    return { id, left, top, width, height }
}

const solve = array => {}

// console.info(solve(require('./3.json')))

module.exports = { solve, parse, Rectangle, Point }
