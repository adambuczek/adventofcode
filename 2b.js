'use strct'

class InputError extends Error {}

const indexedStringDifference = (string1, string2) => {
    if (string1.length !== string2.length) throw new InputError('Only strings of the same lenght can be diffed this way')
    if (string1 === string2) return []
    let diff = []
    for (let i = 0; i < string1.length; i++) {
        if (string1[i] !== string2[i]) {
            diff = [ ...diff, {
                [1]: string1[i],
                [2]: string2[i],
                i,
            }]
        }
    }
    return diff
}

const removeElementByIndex = (sliceable, i) => sliceable.slice(0, i) + sliceable.slice(i + 1)

const solve = array => {
    let tmp
    for (let i = 0; i < array.length; i++) {
        const string1 = array[i]
        for (let j = 0; j < array.length; j++) {
            const string2 = array[j]
            if ((tmp = indexedStringDifference(string1, string2)).length === 1) return removeElementByIndex(string2, tmp[0].i)
        }
    }
}

// console.info(solve(require('./2.json')))

module.exports = {solve, indexedStringDifference, removeElementByIndex}
