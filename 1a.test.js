'use strct'

const solve = require('./1a.js')

const TESTDATA = [-1, -2, -3]
const EXPECTED = -6

it('should return expected ', () => expect(solve(TESTDATA)).toEqual(EXPECTED))
