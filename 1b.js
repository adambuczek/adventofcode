'use strict'

const solve = array => {
    
    const acc = {
        sum: 0,
        frequencies: [],
        firstRepeated: null
    }
    
    for (let i = 0;;) {
        
        const newFrequency = acc.sum + Number(array[i])
        acc.sum = newFrequency

        if (acc.firstRepeated === null && acc.frequencies.includes(newFrequency)) return newFrequency

        acc.frequencies = acc.frequencies.concat(newFrequency)
        i = i === array.length - 1 ? 0 : i + 1
    }

}

// console.info(solve(require('./1.json')))

module.exports = solve
