'use strct'

const solve = array => {
    const summary = array.reduce(
        (acc, string) => {
            const array = string.split('')
            let two = false
            let three = false
    
            // does any element in cur repeat 3 times?
            array.forEach(char => {
                const amount = array.filter(c => c === char).length
    
                if (amount === 3) two = true
                else if (amount === 2) three = true
            })
    
            return {
                two: two ? ++acc.two : acc.two,
                three: three ? ++acc.three : acc.three
            }
        },
        { two: 0, three: 0 }
    )
    return summary.two * summary.three
}

// console.info(solve(require('./2.json')))

module.exports = solve
